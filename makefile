VENV ?= venv
PYTHON ?= $(VENV)/bin/python3
DOCKER ?= docker
GID ?= $(shell id -g)
UID ?= $(shell id -u)


BASE_DIR = $(shell pwd)
PYTHONPATH = $(BASE_DIR)

$(VENV):
	uv venv $(VENV)

.PHONY: compile
compile:
	uv pip compile --python-version 3.9 --emit-index-url --upgrade -o requirements.txt.cpu requirements.in.cpu

requirements.txt.cpu: requirements.in.cpu | $(VENV)
	uv pip compile --python-version 3.9 --emit-index-url --upgrade -o requirements.txt.cpu requirements.in.cpu

.PHONY: docker-build
docker-build:
	$(DOCKER) build -t "dyff/workflows/huggingface-runner-cpu" -f Dockerfile.cpu .
